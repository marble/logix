## tl;dr

- [Hardware](http://git.card10.badge.events.ccc.de/card10/hardware) - hardware repo with docs and specs
- [Firmware](http://git.card10.badge.events.ccc.de/card10/firmware) - firmware repo
- [Firmware Docs](https://firmware.card10.badge.events.ccc.de/) - rendered version of the firmware docs
- [Assembly guide](/en/assembleyourcard10) - Assemble your card10
- [LogBook](/en/logbook) - records of travellers
- [Interhacktions](/en/interhacktions) - a guide to making apps
- [Hardware Overview](/en/hardware-overview)  
- [USB-C](/en/usbc)
- [personal state](/ps)
- [app](/app)
- [assembly video](/vid)