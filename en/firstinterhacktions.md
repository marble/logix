# First interhacktions

The easiest way to make some LEDs blink is using python. There are two ways of executing python code: Using an interactive python command line, and saving `.py` files on the card10. The interactive command line is very practical to try out python commands, and debug '.py' files. Directly saving .py files makes the scripts you write available when you have card10 disconnected from your computer.

## Using the interactive command line
You will be working using the command line. Everything that you need to enter in a command line is written like this: `echo "hello"`
<!-- a link to command line tutorials here would be nice -->

You need:
- a laptop with a free USB-A or USB-C port
- depending on what USB connectors are available on your laptop a USB-A to USB-C cable, or a USB-C to USB-C cable
<!-- usb-A to usb-C and usb-C to usb-C photo would be helpful here -->
- an application for opening a serial connection, e.g. screen (installation via command line:`apt install screen`) or picocom (installation via command line: `apt install picocom`)

### Getting to the interactive python command line
Switch on your card10.

If your card10 display shows the text _USB activated._ your card10 is in the _USB storage_ mode, which is the wrong mode for the interactive python command line. If the device is mounted you need to eject it first. A brief press on the _power_ button will exit the _USB storage_ mode.
<!-- a picture here, indicating the right button would be nice -->

To get to the interactive command line, connect your card10 to your laptop. Typically, it will be called `/dev/ttyACM0` or `/dev/ttyACM1`, on Mac OS X likely `/dev/tty.usbmodem14101`.
You can find out what it is called on your machine by comparing the output of `ls /dev/tty*` (or `ls /dev/tty.*` on Mac OS X) before and after you connect your card10 to the laptop.

Depending on which tool you chose, you can open the interactive python command line. You might need to add "sudo" in front of the commands.
- using screen: `screen /dev/ttyACM0 115200`
- using picocom: `picocom -b 115200 /dev/ttyACM0`


If you don't see a line starting with _>>>_, try pressing _Ctrl+C_ (maybe more than once).

<div class="p-notification--information">
  <p class="p-notification__response">
    <span class="p-notification__status">Note:</span>The first line gives you the version of your firmware, e.g. "MicroPython v1.11-37-g62f004ba4 on 2019-08-17; card10 with max32666".
  </p>
</div>


Once you have a line starting with _>>>_, you are ready to start programming in python!

### Making an LED blink

There are some neat python functions prepared for making the leds blink. Let's import the library containing these functions by typing:

```python
import leds

# Next, we light up one of the r0kets by typing:
>>> leds.set_rocket(0, 31)
```

The first number entered is the index for selecting the r0ket LED. You can choose between 0 (blue, left) , 1 (yellow, middle) and 2 (green, right).


## Using card10 in USB storage mode

TODO: this is how you can turn your commands into a python script and add it to the card10

## Adding your app to the hatchery

Once you have a nice app that runs on your card10, you can share it with the rest of the world by adding it to the hatchery. 
If your app is not quite ready yet, but you want to share it, you can also add it the hatchery and keep it unpublished.
Additionally, the hatchery is a great place to have a look at other people's apps and learn how they implemented their ideas!

Go to the [hatchery](https://badge.team/?badge=card10), register an account, and choose the badge model card10.
Klick on 'Add' to create an egg for your app in the hatchery.  Pick a category, select _card10_ for the compatibility, and add a title and description.
You can even use libraries from existing apps, which you can add as dependencies in the next menu.

Your app comes with an empty `__init__.py` file, this is where your main code goes. You can also upload further libraries and 
configuration files.
When naming additional configuration files, bear in mind that there is a text file reader that displays `.txt` files on the card10 display,
so name your files accordingly if you want them to show up (or not).


Once you are satisfied, you can publish it in an official revision. As you improve your code, you can re-upload your files and publish new versions.

### Closing a session
To close a session in screen, press `Ctrl+A` then `k` then `y`.  In picocom, exit using `Ctrl+A`, then `Ctrl+Q`.

## Congratulations!
When applying your newly learned card10 skills, please have a look at the [general considerations for designing Interhacktions](/en/interhacktions). Also bear in mind that some blinking patterns have been assigned a special meaning for communicating a traveler's [personal state](/ps).
