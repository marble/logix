---
title: Notifications
---


# Overview:

Android notifications can be intercepted and sent to the card10 to be displayed there

# Firmware support:
- The badge can receive notifications via BLE and display them
- It is possible to trigger the  vibration motor with a notification
- It is possible to define on the Android side how the motor vibrates
- It is possible to only show the notification if the user presses one of the buttons 
- Notifications can contain small images (avatar/app icons) to be displayed
- If the text of the notification does not fit on the screen, the buttons on the badge can be used to scroll down (and up)
- User feedback to these notifications is possible via buttons
— For example: If the notification is a call notification: The user can reject or take that call
— In general: pressing a button opens the application on the users phone

# Data Format:
- Protobufs are used  to encode these notifications to the badge


